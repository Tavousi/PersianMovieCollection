<?php /* Smarty version Smarty-3.1-DEV, created on 2014-09-01 17:02:28
         compiled from "C:\WampDeveloper\Websites\test-1.ir\webroot/tpl/default\users\user.html" */ ?>
<?php /*%%SmartyHeaderCode:733054048a843a9b19-10703647%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d7cd0bca7986081c3c2d77f7951a6790b6ddf15' => 
    array (
      0 => 'C:\\WampDeveloper\\Websites\\test-1.ir\\webroot/tpl/default\\users\\user.html',
      1 => 1409060646,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '733054048a843a9b19-10703647',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'User' => 0,
    'username_error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_54048a8449ac0',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54048a8449ac0')) {function content_54048a8449ac0($_smarty_tpl) {?><div class="content">
	<h2><?php echo htmlentities(@EDIT,3,'UTF-8');?>
 <?php echo htmlentities($_smarty_tpl->tpl_vars['user']->value->username,3,'UTF-8');?>
</h2>
	
	<form id="form" method="POST">
	<table>
		<tr>
			<td style="width: 140px;">			
				<?php echo htmlentities(@USER_NAME,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="text" id="username" dir="ltr" name="username" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value)){?><?php echo htmlentities($_smarty_tpl->tpl_vars['user']->value->username,3,'UTF-8');?>
<?php }?>" class="required" disabled style="direction: ltr;text-align: left;">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@EMAIL,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="text" id="email" dir="ltr" name="email" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value)){?><?php echo htmlentities($_smarty_tpl->tpl_vars['user']->value->email,3,'UTF-8');?>
<?php }?>" style="direction: ltr;text-align: left;" class="required email">
			</td>
		</tr>
		
		<tr>
			<td>			
				<?php echo htmlentities(@PASSWORD,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="password" id="password" dir="ltr" name="password" style="direction: ltr;text-align: left;" value="">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@PASSWORD,3,'UTF-8');?>
 (<?php echo htmlentities(@AGAIN,3,'UTF-8');?>
):
			</td>
			<td>
				<input type="password" id="password2" dir="ltr" name="password2" style="direction: ltr;text-align: left;" value="">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@ROLE,3,'UTF-8');?>
:
			</td>
			<td>
				<select name="permission" class="required" style="width: 155px;"<?php if (!$_smarty_tpl->tpl_vars['User']->value->isAdmin()||$_smarty_tpl->tpl_vars['User']->value->id==$_smarty_tpl->tpl_vars['user']->value->id){?> disabled<?php }?>>
					<option value="0"<?php if (isset($_smarty_tpl->tpl_vars['user']->value)&&$_smarty_tpl->tpl_vars['user']->value->permission==0){?> selected<?php }?>><?php echo htmlentities(@GUEST,3,'UTF-8');?>
</option>
					<option value="1"<?php if (isset($_smarty_tpl->tpl_vars['user']->value)&&$_smarty_tpl->tpl_vars['user']->value->permission==1){?> selected<?php }?>><?php echo htmlentities(@EDITOR,3,'UTF-8');?>
</option>
					<option value="2"<?php if (isset($_smarty_tpl->tpl_vars['user']->value)&&$_smarty_tpl->tpl_vars['user']->value->permission==2){?> selected<?php }?>><?php echo htmlentities(@ADMIN,3,'UTF-8');?>
</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;			
				
			</td>
			<td>
				<input type="submit" name="submit" value="<?php echo htmlentities(@SAVE,3,'UTF-8');?>
" class="button">
			</td>
		</tr>
		<?php if (isset($_smarty_tpl->tpl_vars['username_error']->value)){?>
		<tr>
			<td colspan="2" class="error">			
				<?php echo htmlentities($_smarty_tpl->tpl_vars['username_error']->value,3,'UTF-8');?>

			</td>
		</tr>
		<?php }?>
	</table>
	</form>
	
	<script>
		$(document).ready(function() {
			$.validator.messages["required"] = "<?php echo htmlentities(@VALIDATOR_REQUIRED,3,'UTF-8');?>
";
			$.validator.messages["email"] = "<?php echo htmlentities(@VALIDATOR_EMAIL,3,'UTF-8');?>
";
			$.validator.messages["equalTo"] = "<?php echo @VALIDATOR_EQUAL_TO;?>
";
			$("#form").validate({
				rules: {
					password2: {
			    		equalTo: "#password"
					}
				}
			});
		});
		$("#email").focus();
	</script>
</div><?php }} ?>