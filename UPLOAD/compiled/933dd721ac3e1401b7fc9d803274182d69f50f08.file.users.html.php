<?php /* Smarty version Smarty-3.1-DEV, created on 2014-09-01 17:02:46
         compiled from "C:\WampDeveloper\Websites\test-1.ir\webroot/tpl/default\users\users.html" */ ?>
<?php /*%%SmartyHeaderCode:1566754048a96a52e22-37346960%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '933dd721ac3e1401b7fc9d803274182d69f50f08' => 
    array (
      0 => 'C:\\WampDeveloper\\Websites\\test-1.ir\\webroot/tpl/default\\users\\users.html',
      1 => 1409060685,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1566754048a96a52e22-37346960',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'newuser' => 0,
    'username_error' => 0,
    'users' => 0,
    'tpl_include' => 0,
    'User' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_54048a96c0396',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54048a96c0396')) {function content_54048a96c0396($_smarty_tpl) {?><div class="content">
	<h2><?php echo htmlentities(@USER_MANAGEMENT,3,'UTF-8');?>
 : <?php echo htmlentities(@NEW_USER,3,'UTF-8');?>
</h2>
		
	<form id="form" method="POST">
	<table>
		<tr>
			<td style="width: 140px;">			
				<?php echo htmlentities(@USER_NAME,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="text" id="username" name="username" value="<?php if (isset($_smarty_tpl->tpl_vars['newuser']->value)){?><?php echo htmlentities($_smarty_tpl->tpl_vars['newuser']->value->username,3,'UTF-8');?>
<?php }?>" style="direction: ltr;text-align: left;" class="required">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@EMAIL,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="text" name="email" value="<?php if (isset($_smarty_tpl->tpl_vars['newuser']->value)){?><?php echo htmlentities($_smarty_tpl->tpl_vars['newuser']->value->email,3,'UTF-8');?>
<?php }?>" style="direction: ltr;text-align: left;" class="required email">
			</td>
		</tr>
		
		<tr>
			<td>			
				<?php echo htmlentities(@PASSWORD,3,'UTF-8');?>
:
			</td>
			<td>
				<input type="password" id="password" name="password" value="" style="direction: ltr;text-align: left;">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@PASSWORD,3,'UTF-8');?>
 (<?php echo htmlentities(@AGAIN,3,'UTF-8');?>
):
			</td>
			<td>
				<input type="password" id="password2" name="password2" value="" style="direction: ltr;text-align: left;">
			</td>
		</tr>
		<tr>
			<td>			
				<?php echo htmlentities(@ROLE,3,'UTF-8');?>
:
			</td>
			<td>
				<select name="permission" class="required" style="width: 155px;">
					<option value="0"<?php if (isset($_smarty_tpl->tpl_vars['newuser']->value)&&$_smarty_tpl->tpl_vars['newuser']->value->permission==0){?> selected<?php }?>><?php echo htmlentities(@GUEST,3,'UTF-8');?>
</option>
					<option value="1"<?php if (isset($_smarty_tpl->tpl_vars['newuser']->value)&&$_smarty_tpl->tpl_vars['newuser']->value->permission==1){?> selected<?php }?>><?php echo htmlentities(@EDITOR,3,'UTF-8');?>
</option>
					<option value="2"<?php if (isset($_smarty_tpl->tpl_vars['newuser']->value)&&$_smarty_tpl->tpl_vars['newuser']->value->permission==2){?> selected<?php }?>><?php echo htmlentities(@ADMIN,3,'UTF-8');?>
</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;			
				
			</td>
			<td>
				<input type="submit" name="submit" value="<?php echo htmlentities(@SAVE,3,'UTF-8');?>
" class="button">
			</td>
		</tr>
		<?php if (isset($_smarty_tpl->tpl_vars['username_error']->value)){?>
		<tr>
			<td colspan="2" class="error">			
				<?php echo htmlentities($_smarty_tpl->tpl_vars['username_error']->value,3,'UTF-8');?>

			</td>
		</tr>
		<?php }?>
	</table>
	</form>
	
	<script>
		$(document).ready(function() {
			$.validator.messages["required"] = "<?php echo htmlentities(@VALIDATOR_REQUIRED,3,'UTF-8');?>
";
			$.validator.messages["email"] = "<?php echo htmlentities(@VALIDATOR_EMAIL,3,'UTF-8');?>
";
			$.validator.messages["equalTo"] = "<?php echo @VALIDATOR_EQUAL_TO;?>
";
			$("#form").validate({
				rules: {
					password: "required",
					password2: {
			    		equalTo: "#password"
					}
				}
			});
		});
		$("#username").focus();
	</script>
</div>

<div class="content">
	<h3><?php echo htmlentities(@USERS,3,'UTF-8');?>
</h3>
	
	<table>
		<thead>
			<tr>
				<td style="width: 20px;">&nbsp;</td>
				<td><?php echo htmlentities(@USER_NAME,3,'UTF-8');?>
</td>
				<td><?php echo htmlentities(@EMAIL,3,'UTF-8');?>
</td>
				<td><?php echo htmlentities(@ROLE,3,'UTF-8');?>
</td>
				<td style="width: 200px;"><?php echo htmlentities(@LAST_LOGGED_IN,3,'UTF-8');?>
</td>
				<td style="width: 20px;">&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['u'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['u']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['name'] = 'u';
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['users']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['u']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['u']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['u']['total']);
?>
			<tr>
				<td>
					<a href="./?go=user&id=<?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->id;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/16x16/user_edit.png" alt="<?php echo htmlentities(@EDIT,3,'UTF-8');?>
" title="<?php echo htmlentities(@EDIT,3,'UTF-8');?>
"></a>
				</td>
				<td>
					<a href="./?go=user&id=<?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->id;?>
">
					<?php echo htmlentities($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->username,3,'UTF-8');?>

					</a>
				</td>
				<td>
					<a href="mailto:<?php echo htmlentities($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->email,3,'UTF-8');?>
">
					<?php echo htmlentities($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->email,3,'UTF-8');?>

					</a>
				</td>
				<td>
					<?php if ($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->permission==0){?><?php echo htmlentities(@GUEST,3,'UTF-8');?>
<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->permission==1){?><?php echo htmlentities(@EDITOR,3,'UTF-8');?>
<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->permission==2){?><?php echo htmlentities(@ADMIN,3,'UTF-8');?>
<?php }?>
				</td>
				<td>
					<?php if ($_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->lastlogin!='0000-00-00 00:00:00'){?><?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->lastlogin;?>
<?php }?>
				</td>
				<td>
					<?php if ($_smarty_tpl->tpl_vars['User']->value->isAdmin()&&$_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->username!="admin"){?>
					<a href="javascript:void(0);" onclick="if(confirm('<?php echo htmlentities(@CONFIRM_REMOVE,3,'UTF-8');?>
')) location.href='./?go=deleteuser&id=<?php echo $_smarty_tpl->tpl_vars['users']->value[$_smarty_tpl->getVariable('smarty')->value['section']['u']['index']]->id;?>
';"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/16x16/delete.png" alt="<?php echo htmlentities(@REMOVE,3,'UTF-8');?>
" title="<?php echo htmlentities(@REMOVE,3,'UTF-8');?>
"></a>
					<?php }?>
				</td>
			</tr>
			<?php endfor; endif; ?>
		</tbody>
	</table>
</div><?php }} ?>