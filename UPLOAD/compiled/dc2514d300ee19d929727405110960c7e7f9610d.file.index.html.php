<?php /* Smarty version Smarty-3.1-DEV, created on 2014-08-31 09:04:38
         compiled from "C:\WampDeveloper\Websites\test-1.ir\webroot/tpl/default\index.html" */ ?>
<?php /*%%SmartyHeaderCode:179645402c906ca5ab2-83427646%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc2514d300ee19d929727405110960c7e7f9610d' => 
    array (
      0 => 'C:\\WampDeveloper\\Websites\\test-1.ir\\webroot/tpl/default\\index.html',
      1 => 1409291983,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '179645402c906ca5ab2-83427646',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'languages' => 0,
    'language' => 0,
    'code' => 0,
    'name' => 0,
    'loggedin' => 0,
    'guestview' => 0,
    'currentUrl' => 0,
    'webroot' => 0,
    'User' => 0,
    'main' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5402c906da41d',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5402c906da41d')) {function content_5402c906da41d($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div id="container">
	<div id="wrapper">
		<header>
			<div class="center">
				<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1){?>
				<div class="languages">
                <span><?php echo htmlentities(@chenge_lng,3,'UTF-8');?>
</span>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['code']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['code']->key => $_smarty_tpl->tpl_vars['code']->value){
$_smarty_tpl->tpl_vars['code']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['code']->key;
?>
						<li<?php if ($_smarty_tpl->tpl_vars['language']->value==$_smarty_tpl->tpl_vars['code']->value){?> class="selected"<?php }?>><a href="./?lang=<?php echo urlencode($_smarty_tpl->tpl_vars['code']->value);?>
"><?php echo htmlentities($_smarty_tpl->tpl_vars['name']->value,3,'UTF-8');?>
</a></li>
					<?php } ?>
					</ul>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['loggedin']->value||$_smarty_tpl->tpl_vars['guestview']->value){?>
				<nav>
					<ul>
						<li<?php if ($_smarty_tpl->tpl_vars['currentUrl']->value==$_smarty_tpl->tpl_vars['webroot']->value){?> class="selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
"><?php echo htmlentities(@HOME,3,'UTF-8');?>
</a></li>
						<?php if ($_smarty_tpl->tpl_vars['loggedin']->value){?>
						<li<?php if ($_smarty_tpl->tpl_vars['currentUrl']->value==($_smarty_tpl->tpl_vars['webroot']->value).("?go=profile")){?> class="selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
?go=profile"><?php echo htmlentities(@MY_PROFILE,3,'UTF-8');?>
</a></li>
						<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isAdmin()){?><li<?php if ($_smarty_tpl->tpl_vars['currentUrl']->value==($_smarty_tpl->tpl_vars['webroot']->value).("?go=users")){?> class="selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
?go=users"><?php echo htmlentities(@USER_MANAGEMENT,3,'UTF-8');?>
</a></li><?php }?>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
?logout" onclick="return confirm('<?php echo htmlentities(@ARE_YOU_SURE_YOU_WANT_TO_LOG_OUT,3,'UTF-8');?>
');"><?php echo htmlentities(@LOG_OUT,3,'UTF-8');?>
</a></li>
						<?php }else{ ?>
						<li<?php if ($_smarty_tpl->tpl_vars['currentUrl']->value==($_smarty_tpl->tpl_vars['webroot']->value).("?go=login")){?> class="selected"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
?go=login&ref=<?php echo urlencode($_smarty_tpl->tpl_vars['currentUrl']->value);?>
"><?php echo htmlentities(@LOG_IN,3,'UTF-8');?>
</a></li>
						<?php }?>
					</ul>
				</nav>
				<?php }?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['webroot']->value;?>
"><h1><?php echo htmlentities(@_TITLE,3,'UTF-8');?>

                <br /><?php echo htmlentities(@MY_COLLECTION,3,'UTF-8');?>
</h1>
                </a>
			</div>
		</header>
		
		<div id="content">
			<section>
				<?php if (!$_smarty_tpl->tpl_vars['loggedin']->value&&!$_smarty_tpl->tpl_vars['guestview']->value){?>
					<?php echo $_smarty_tpl->getSubTemplate ("users/login.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				<?php }?>	
				<?php if ($_smarty_tpl->tpl_vars['loggedin']->value||$_smarty_tpl->tpl_vars['guestview']->value){?>
					<?php if (isset($_smarty_tpl->tpl_vars['main']->value)){?><?php echo $_smarty_tpl->getSubTemplate (($_smarty_tpl->tpl_vars['main']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }?>
				<?php }?>
			</section>
		</div>
	</div>
    
    <div style="clear:both;"></div>
	<footer>
		<a href="/" target="_blank"><?php echo htmlentities(@footer_msg,3,'UTF-8');?>
</a>
	</footer>
    	
</div>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?>
<div class="message">
	<div class="text">
		<?php echo htmlentities(translate($_smarty_tpl->tpl_vars['message']->value),3,'UTF-8');?>

	</div>
</div>
<?php }?>
    
<?php echo $_smarty_tpl->getSubTemplate ("footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>