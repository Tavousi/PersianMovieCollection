<?php /* Smarty version Smarty-3.1-DEV, created on 2014-09-01 17:01:05
         compiled from "C:\WampDeveloper\Websites\test-1.ir\webroot/tpl/default\movies\movie.html" */ ?>
<?php /*%%SmartyHeaderCode:3000854041c9fb75c75-57127587%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd42563ec0d33961d4248b48ae250dd4546f7808' => 
    array (
      0 => 'C:\\WampDeveloper\\Websites\\test-1.ir\\webroot/tpl/default\\movies\\movie.html',
      1 => 1409583662,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3000854041c9fb75c75-57127587',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_54041ca053456',
  'variables' => 
  array (
    'movie' => 0,
    'tpl_include' => 0,
    'loggedin' => 0,
    'User' => 0,
    'imdbmovie' => 0,
    'taglines' => 0,
    'photo' => 0,
    'genres' => 0,
    'languages' => 0,
    'subtitles' => 0,
    'country' => 0,
    'director' => 0,
    'writer' => 0,
    'producer' => 0,
    'music' => 0,
    'video' => 0,
    'audio' => 0,
    'plotoutline' => 0,
    'cast' => 0,
    'plots' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54041ca053456')) {function content_54041ca053456($_smarty_tpl) {?><nav>
	<ul>
		<?php if ($_smarty_tpl->tpl_vars['movie']->value->imdbid!=''){?>
		<li>
			<div class="button">
				<div>
					<a href="http://www.imdb.com/title/tt<?php echo $_smarty_tpl->tpl_vars['movie']->value->imdbid;?>
/" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/imdb.png" alt="<?php echo htmlentities(@VISIT_IMDB,3,'UTF-8');?>
" title="<?php echo htmlentities(@VISIT_IMDB,3,'UTF-8');?>
"><br/>
					<div style="margin-top: -6px;"><?php echo @VISIT_IMDB;?>
</div></a>
				</div>
			</div>
		</li>
		<?php }?>
			
		<?php if ($_smarty_tpl->tpl_vars['movie']->value->trailer){?>
		<li style="margin-left: 15px;margin-right: 15px;">
			<div class="button">
				<div>
					<a href="<?php echo $_smarty_tpl->tpl_vars['movie']->value->trailer;?>
<?php if (!$_smarty_tpl->tpl_vars['movie']->value->getYouTubeTrailerId()){?><?php if (strpos($_smarty_tpl->tpl_vars['movie']->value->trailer,"?")>0){?>&<?php }else{ ?>?<?php }?>iframe=true&width=100%&height=100%<?php }?>" rel="prettyPhoto<?php if (!$_smarty_tpl->tpl_vars['movie']->value->getYouTubeTrailerId()){?>[iframes]<?php }?>" title="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
 (<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->year,3,'UTF-8');?>
)">
					<img style="margin-right: 15px;" src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/movies.png" alt="<?php echo htmlentities(@VIEW_TRAILER,3,'UTF-8');?>
" title="<?php echo htmlentities(@VIEW_TRAILER,3,'UTF-8');?>
"><br/>
					<div style="margin-top: -6px;width: 90px;"><?php echo htmlentities(@VIEW_TRAILER,3,'UTF-8');?>
</div></a>
				</div>
			</div>
		</li>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['movie']->value->hasCover()){?>
		<li style="margin-left: 15px;">
			<div class="button">
				<div>
					<a href="./?go=downloadcover&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
"><img style="margin-right: 30px;" src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/picture_save.png" alt="<?php echo htmlentities(@DOWNLOAD_COVER,3,'UTF-8');?>
" title="<?php echo htmlentities(@DOWNLOAD_COVER,3,'UTF-8');?>
"><br/>
					<div style="margin-top: -6px;width: 120px;margin-right: -12px;"><?php echo htmlentities(@DOWNLOAD_COVER,3,'UTF-8');?>
</div></a>
				</div>
			</div>
		</li>
		<?php }?>
			
		<li style="margin-left: -10px;">
			<div class="button">
				<div>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					<a href="./?go=own&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
&own=<?php if ($_smarty_tpl->tpl_vars['movie']->value->own){?>0<?php }else{ ?>1<?php }?>">
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['movie']->value->own){?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/star.png" alt="<?php echo htmlentities(@OWN,3,'UTF-8');?>
" title="<?php echo htmlentities(@OWN,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@OWN,3,'UTF-8');?>
</div>
					<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/star_gray.png" alt="<?php echo htmlentities(@NOT_OWN,3,'UTF-8');?>
" title="<?php echo htmlentities(@NOT_OWN,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@NOT_OWN,3,'UTF-8');?>
</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					</a>
					<?php }?>
				</div>
			</div>
		</li>
		
		<li>
			<div class="button">
				<div>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					<a href="./?go=own&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
&seen=<?php if ($_smarty_tpl->tpl_vars['movie']->value->seen){?>0<?php }else{ ?>1<?php }?>">
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['movie']->value->seen){?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/eye.png" alt="<?php echo htmlentities(@SEEN,3,'UTF-8');?>
" title="<?php echo htmlentities(@SEEN,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@SEEN,3,'UTF-8');?>
</div>
					<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/eye_gray.png" alt="<?php echo htmlentities(@UNSEEN,3,'UTF-8');?>
" title="<?php echo htmlentities(@UNSEEN,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@UNSEEN,3,'UTF-8');?>
</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					</a>
					<?php }?>
				</div>
			</div>
		</li>

		<li>
			<div class="button">
				<div>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					<a href="./?go=own&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
&dubbed=<?php if ($_smarty_tpl->tpl_vars['movie']->value->dubbed){?>0<?php }else{ ?>1<?php }?>">
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['movie']->value->dubbed){?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/file_extension_wav.png" alt="<?php echo htmlentities(@dubbed1,3,'UTF-8');?>
" title="<?php echo htmlentities(@dubbed1,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@dubbed1,3,'UTF-8');?>
</div>
					<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/file_extension_wav_gray.png" alt="<?php echo htmlentities(@dubbed0,3,'UTF-8');?>
" title="<?php echo htmlentities(@dubbed0,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;"><?php echo htmlentities(@dubbed0,3,'UTF-8');?>
</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					</a>
					<?php }?>
				</div>
			</div>
		</li>
        		
		<li style="margin-left: 10px;margin-right: 20px;">
			<div class="button">
				<div>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					<a href="./?go=own&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
&sebodi=<?php if ($_smarty_tpl->tpl_vars['movie']->value->sebodi){?>0<?php }else{ ?>1<?php }?>">
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['movie']->value->sebodi){?>
						<img style="margin-right: 10px;" src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/3d_glasses.png" alt="<?php echo htmlentities(@sebodi1,3,'UTF-8');?>
" title="<?php echo htmlentities(@sebodi1,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;width: 80px;"><?php echo htmlentities(@sebodi1,3,'UTF-8');?>
</div>
					<?php }else{ ?>
						<img style="margin-right: 10px;" src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/3d_glasses_gray.png" alt="<?php echo htmlentities(@sebodi0,3,'UTF-8');?>
" title="<?php echo htmlentities(@sebodi0,3,'UTF-8');?>
"><br/>
						<div style="margin-top: -6px;width: 80px;"><?php echo htmlentities(@sebodi0,3,'UTF-8');?>
</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
					</a>
					<?php }?>
				</div>
			</div>
		</li>
        		       
		<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
		<li>
			<div class="button">
				<div>
					<a href="./?go=edit&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/pencil.png" alt="<?php echo htmlentities(@EDIT,3,'UTF-8');?>
" title="<?php echo htmlentities(@EDIT,3,'UTF-8');?>
"><br/>
					<div style="margin-top: -6px;"><?php echo htmlentities(@EDIT,3,'UTF-8');?>
</div></a>
				</div>
			</div>
		</li>
		<?php }?>
			
		<?php if ($_smarty_tpl->tpl_vars['loggedin']->value&&$_smarty_tpl->tpl_vars['User']->value->isEditor()){?>
		<li>
			<div class="button">
				<div>
					<a href="javascript:void(0);" onclick="if(confirm('<?php echo htmlentities(@CONFIRM_REMOVE,3,'UTF-8');?>
')) location.href='./?go=delete&id=<?php echo $_smarty_tpl->tpl_vars['movie']->value->id;?>
';"><img src="<?php echo $_smarty_tpl->tpl_vars['tpl_include']->value;?>
images/icons/32x32/delete.png" alt="<?php echo htmlentities(@REMOVE,3,'UTF-8');?>
" title="<?php echo htmlentities(@REMOVE,3,'UTF-8');?>
"><br/>
					<div style="margin-top: -6px;"><?php echo htmlentities(@REMOVE,3,'UTF-8');?>
</div></a>
				</div>
			</div>
		</li>
		<?php }?>
	</ul>
</nav>

<div class="content">
	<div id="movie">
		
		<div class="photo">
			<?php $_smarty_tpl->tpl_vars['photo'] = new Smarty_variable(0, null, 0);?>
			<?php if (isset($_smarty_tpl->tpl_vars['imdbmovie']->value)&&$_smarty_tpl->tpl_vars['imdbmovie']->value->photo()){?>
			<img src="<?php echo $_smarty_tpl->tpl_vars['imdbmovie']->value->photo();?>
" alt="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
" title="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
">
			<?php $_smarty_tpl->tpl_vars['photo'] = new Smarty_variable(1, null, 0);?>
			<?php }elseif(isset($_smarty_tpl->tpl_vars['movie']->value)&&$_smarty_tpl->tpl_vars['movie']->value->hasPhoto()){?>
			<img src="<?php echo $_smarty_tpl->tpl_vars['movie']->value->photo();?>
" alt="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
" title="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
">
			<?php $_smarty_tpl->tpl_vars['photo'] = new Smarty_variable(1, null, 0);?>
			<?php }elseif(isset($_smarty_tpl->tpl_vars['movie']->value)&&$_smarty_tpl->tpl_vars['movie']->value->hasCover()){?>
			<img src="<?php echo $_smarty_tpl->tpl_vars['movie']->value->cover();?>
" alt="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
" title="<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
">
			<?php $_smarty_tpl->tpl_vars['photo'] = new Smarty_variable(1, null, 0);?>
			<?php }?>
		
			<div class="taglines">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->taglines&&strlen($_smarty_tpl->tpl_vars['movie']->value->taglines)>0){?>
				<?php $_smarty_tpl->tpl_vars['taglines'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('taglines'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['t'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['t']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['name'] = 't';
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['taglines']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['max'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['t']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['t']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['t']['total']);
?>
					<div><?php echo htmlentities($_smarty_tpl->tpl_vars['taglines']->value[$_smarty_tpl->getVariable('smarty')->value['section']['t']['index']],3,'UTF-8');?>
</div>
				<?php endfor; endif; ?>
				<?php }?>
			</div>
		</div>
		
		<div class="maininfo<?php if (!$_smarty_tpl->tpl_vars['photo']->value){?>_full<?php }?>">
			<h2>
				<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->name,3,'UTF-8');?>
 محصول سال <?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->year,3,'UTF-8');?>

			</h2>
		
			<div class="loaned">
			<?php if ($_smarty_tpl->tpl_vars['movie']->value->loaned){?>
				<?php echo htmlentities(@LOANED_OUT,3,'UTF-8');?>
<?php if ($_smarty_tpl->tpl_vars['movie']->value->loanname!=''){?> <?php echo htmlentities(@TO,3,'UTF-8');?>
 <?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->loanname,3,'UTF-8');?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['movie']->value->loandate!="0000-00-00"){?> <?php echo htmlentities(@ON,3,'UTF-8');?>
 <?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->loandate,3,'UTF-8');?>
<?php }?>
			<?php }?>
			</div>
			
			<div class="genre">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->genres&&strlen($_smarty_tpl->tpl_vars['movie']->value->genres)>0){?>
				<?php $_smarty_tpl->tpl_vars['genres'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('genres'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['g'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['g']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['name'] = 'g';
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['genres']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['g']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['g']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['g']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['g']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['genres']->value[$_smarty_tpl->getVariable('smarty')->value['section']['g']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="duration">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->duration&&$_smarty_tpl->tpl_vars['movie']->value->duration!=0){?><?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->duration,3,'UTF-8');?>
 <?php echo htmlentities(@MINUTES,3,'UTF-8');?>
<?php }?>
			</div>
			
			<div class="rating">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->rating&&$_smarty_tpl->tpl_vars['movie']->value->rating!=0){?>
				<span class="header"><?php echo htmlentities(@RATING,3,'UTF-8');?>
:</span> 
				<?php echo htmlentities($_smarty_tpl->tpl_vars['movie']->value->rating,3,'UTF-8');?>

				<?php }?>
			</div>
			
			<div class="languages">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->languages&&strlen($_smarty_tpl->tpl_vars['movie']->value->languages)>0){?>
				<span class="header"><?php echo htmlentities(@LANGUAGES,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['languages'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('languages'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['l'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['l']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['name'] = 'l';
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['languages']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['l']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['l']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['l']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['l']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['languages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['l']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="subtitles">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->subtitles&&strlen($_smarty_tpl->tpl_vars['movie']->value->subtitles)>0){?>
				<span class="header"><?php echo htmlentities(@SUBTITLES,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['subtitles'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('subtitles'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['s'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['s']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['name'] = 's';
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['subtitles']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['s']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['s']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['s']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['s']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['subtitles']->value[$_smarty_tpl->getVariable('smarty')->value['section']['s']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="country">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->country&&strlen($_smarty_tpl->tpl_vars['movie']->value->country)>0){?>
				<span class="header"><?php echo htmlentities(@COUNTRY,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['country'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('country'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['c'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['c']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['name'] = 'c';
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['c']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['c']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="director">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->director&&strlen($_smarty_tpl->tpl_vars['movie']->value->director)>0){?>
				<span class="header"><?php echo htmlentities(@DIRECTOR,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['director'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('director'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['d'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['d']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['name'] = 'd';
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['director']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['d']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['d']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['d']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['d']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['director']->value[$_smarty_tpl->getVariable('smarty')->value['section']['d']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="writer">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->writer&&strlen($_smarty_tpl->tpl_vars['movie']->value->writer)>0){?>
				<span class="header"><?php echo htmlentities(@WRITER,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['writer'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('writer'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['w'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['w']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['name'] = 'w';
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['writer']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['w']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['w']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['w']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['w']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['writer']->value[$_smarty_tpl->getVariable('smarty')->value['section']['w']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="producer">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->producer&&strlen($_smarty_tpl->tpl_vars['movie']->value->producer)>0){?>
				<span class="header"><?php echo htmlentities(@PRODUCER,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['producer'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('producer'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['p'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['p']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['name'] = 'p';
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['producer']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['p']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['producer']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="music">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->music&&strlen($_smarty_tpl->tpl_vars['movie']->value->music)>0){?>
				<span class="header"><?php echo htmlentities(@MUSIC,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['music'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('music'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['m'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['m']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['name'] = 'm';
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['music']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['m']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['m']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['m']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['m']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['music']->value[$_smarty_tpl->getVariable('smarty')->value['section']['m']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="video">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->video&&strlen($_smarty_tpl->tpl_vars['movie']->value->video)>0){?>
				<span class="header"><?php echo htmlentities(@VIDEO,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['video'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('video'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['v'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['v']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['name'] = 'v';
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['video']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['v']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['v']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['v']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['v']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['video']->value[$_smarty_tpl->getVariable('smarty')->value['section']['v']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="audio">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->audio&&strlen($_smarty_tpl->tpl_vars['movie']->value->audio)>0){?>
				<span class="header"><?php echo htmlentities(@AUDIO,3,'UTF-8');?>
:</span>
				<?php $_smarty_tpl->tpl_vars['audio'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('audio'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['a'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['a']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['name'] = 'a';
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['audio']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['a']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['a']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['a']['total']);
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['section']['a']['first']){?>|<?php }?>
					<?php echo htmlentities($_smarty_tpl->tpl_vars['audio']->value[$_smarty_tpl->getVariable('smarty')->value['section']['a']['index']],3,'UTF-8');?>

				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="notes">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->notes&&strlen($_smarty_tpl->tpl_vars['movie']->value->notes)>0){?>
				<h3><?php echo htmlentities(@PERSONAL_NOTES,3,'UTF-8');?>
</h3>
				<?php echo nl2br(htmlentities($_smarty_tpl->tpl_vars['movie']->value->notes,3,'UTF-8'));?>

				<?php }?>
			</div>
			
			<div class="plotoutline">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->plotoutline&&strlen($_smarty_tpl->tpl_vars['movie']->value->plotoutline)>0){?>
				<h3><?php echo htmlentities(@PLOT_OUTLINE,3,'UTF-8');?>
</h3>
				<?php $_smarty_tpl->tpl_vars['plotoutline'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('plotoutline'), null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['p'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['p']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['name'] = 'p';
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['plotoutline']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total']);
?>
					<div><?php echo htmlentities($_smarty_tpl->tpl_vars['plotoutline']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']],3,'UTF-8');?>
</div>
				<?php endfor; endif; ?>
				<?php }?>
			</div>
			
			<div class="cast">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->cast&&strlen($_smarty_tpl->tpl_vars['movie']->value->cast)>0){?>
				<h3><?php echo htmlentities(@CAST,3,'UTF-8');?>
</h3>
				<div class="castlist">
					<ul>
					<?php $_smarty_tpl->tpl_vars['cast'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('cast'), null, 0);?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['c'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['c']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['name'] = 'c';
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['cast']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['c']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['c']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['c']['total']);
?>
						<li><?php echo htmlentities($_smarty_tpl->tpl_vars['cast']->value[$_smarty_tpl->getVariable('smarty')->value['section']['c']['index']],3,'UTF-8');?>
</li>
					<?php endfor; endif; ?>
					<?php }?>
					</ul>
				</div>
			</div>
			
			<div class="plots">
				<?php if ($_smarty_tpl->tpl_vars['movie']->value->plots&&strlen($_smarty_tpl->tpl_vars['movie']->value->plots)>0){?>
				<h3><?php echo htmlentities(@PLOTS,3,'UTF-8');?>
</h3>
				<ol>
					<?php $_smarty_tpl->tpl_vars['plots'] = new Smarty_variable($_smarty_tpl->tpl_vars['movie']->value->getList('plots'), null, 0);?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['p'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['p']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['name'] = 'p';
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['plots']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total']);
?>
						<li><?php echo htmlentities($_smarty_tpl->tpl_vars['plots']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']],3,'UTF-8');?>
</li>
					<?php endfor; endif; ?>
				</ol>
				<?php }?>
			</div>
		</div>
		
		<div style="clear: both"></div>
	</div>
</div>


<script>
$(document).ready(function() {
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: '',
		default_width: 800,
		default_height: 550
	});
	$(".castlist").columnize({
		columns: 4
	});
});
</script>
<?php }} ?>