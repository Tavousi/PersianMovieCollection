<?php
/**
 * This is the language file. If you want the website to be in your own language, translate the following lines and
 * change the configuration settings where you add the new language and update the default language:
 * 
 * config.php:
 * $settings["languages"] = array("English" => "en_US"); -> $settings["languages"] = array("English" => "en_US", "Nederlands" => "nl_NL");
 * $settings["defaultlanguage"] = "en_US" -> $settings["defaultlanguage"] = "nl_NL";
 * 
 * When you want your translation to be included in the next php4dvd release, please send me a message on http://sourceforge.net/projects/php4dvd/
 */

/**
 * Title
 */
define("_TITLE",									"ایـــران فیلم");
/**
 * Menu
 */
define("MY_COLLECTION",								"بزرگترین سایت دانلود فیلم و سریال");
define("HOME",										"صفحه اصلی");
define("MY_PROFILE",								"پروفایل من");
define("USER_MANAGEMENT",							"مدیریت کاربران");
define("LOG_IN", 									"ورود به سایت");
define("signup", 									"عضویت در سایت");
define("LOG_OUT",									"خروج از سایت");
define("ARE_YOU_SURE_YOU_WANT_TO_LOG_OUT",			"آیا شما مطمن هستید می خواهید خارج شوید ؟");
/**
 * Log in page
 */
define("USER_NAME",									"نام کاربری");
define("PASSWORD",									"رمزعبور");
define("INCORRECT_USERNAME_OR_PASSWORD",			"نام کاربری و رمزعبور شما صحیح نمی باشد .");
/**
 * Home
 */
// Lang
define("chenge_lng",								"انتخاب زبان سایت :");
// Menu
define("ADD_MOVIE",									"افزودن فیلم");
define("UPDATE_ALL_MOVIE_INFORMATION",				"بروزرسانی همه");
define("EXPORT_TO_CSV",								"دریافت لیست فیلم ها");
// Search
define("SEARCH_DEFAULT_TEXT",						"جستجو در فیلم ها ...");
define("ALL_CATEGORIES",							"همه موضوعات");
define("SORT_BY",									"جدا سازی بر اساس");
define("name asc",									"نام فیلم (A-Z)");
define("name desc",									"نام فیلم (Z-A)");
define("year asc",									"سال تولید (0-9)");
define("year desc",									"سال تولید (9-0)");
define("rating asc",								"امتیاز (0-9)");
define("rating desc",								"امتیاز (9-0)");
define("format asc",								"کیفیت (A-Z)");
define("format desc",								"کیفیت (Z-A)");
define("seen asc",									"دیده شده ها");
define("seen desc",									"ندیده شده ها");
define("own asc",									"داشته ها");
define("own desc",									"نداشته ها");
define("added asc",									"قدیم به جدید");
define("added desc",								"جدید به قدیم");
define("loaned asc",								"کرایه از قدیم به جدید");
define("loaned desc",								"کرایه از جدید به قدیم");
define("dubbed asc",								"دوبله از قدیم به جدید");
define("dubbed desc",								"دوبله جدید به قدیم");
define("sebodi asc",								"سه بعدی از قدیم به جدید");
define("sebodi desc",								"سه بعدی از جدید به قدیم");
define("ALL", 										"همه");
define("RESULTS_PER_PAGE",							"فیلم در هر صفحه");
// Results
define("NO_RESULTS_FOUND",							"هیچ فیلمی یافت نشد .");
define("NO_COVER",									"بدون کاور");
/**
 * Movie
 */
// Menu
define("VISIT_IMDB",								"مشاهده IMDB");
define("VIEW_TRAILER",								"نمایش تبلیغ فیلم");
define("DOWNLOAD_COVER",							"دانلود کاور فیلم");
define("OWN",										"دارم");
define("NOT_OWN",									"ندارم");
define("SEEN",										"دیدم");
define("UNSEEN",									"ندیدم");
define("EDIT",										"ویرایش");
define("REMOVE",									"حذف");
// Movie information
define("LOANED_OUT",								"کرایه شده");
define("TO",										"این");
define("ON",										"در");
define("MINUTES",									"دقیقه");
/**
* Add/edit movie
*/
// Menu
define("SAVE",										"ذخیره");
define("UPDATE",									"بروزرسانی");
define("REMOVE_COVER",								"حذف کاور");
// IMDb search
define("ADD_FROM_IMDB",								"افزودن از IMDB");
define("SEARCH",									"جستجو");
define("RESULTS_FROM_IMDB",							"نتایج در IMDB");
// Movie information
define("MOVIE_INFORMATION",							"اطلاعات فیلم");
define("IMDB_NUMBER",								"شماره IMDB");
define("TITLE",										"نام فیلم");
define("AKA_TITLES",								"اسامی دیگر فیلم");
define("YEAR",										"سال تولید");
define("DURATION_MINUTES",							"مدت زمان فیلم");
define("RATING",									"امتیاز IMDb");
define("FORMAT",									"کیفیت فیلم");
define("DVD",										"بلو-ری");
define("I_HAVE_SEEN_THIS_MOVIE",					"من این فیلم را دیدم");
define("I_OWN_THIS_MOVIE",							"من این فیلم را دارم");
define("LOANED_OUT_TO",								"کرایه تا");
define("LOANED_OUT_SINCE",							"کرایه از");
define("YES",										"بله");
define("NO",										"خیر");
define("COVER",										"کاور");
define("SEARCH_FOR_COVER",							"جستجو برای کاور فیلم");
define("TRAILER_URL",								"آدرس تبلیغ");
define("SEARCH_FOR_TRAILER",						"جستجو برای تبلیغ فیلم");
define("PERSONAL_NOTES",							"معرفی و نقد فیلم از دیدگاه سایت");
define("TAGLINES",									"Taglines");
define("PLOT_OUTLINE",								"خلاصه داستان فیلم");
define("PLOTS",										"حواشی فیلم");
define("LANGUAGES",									"زبان اصلی فیلم");
define("SUBTITLES",									"زیرنویس موجود فیلم");
define("AUDIO",										"صوت های موجود فیلم");
define("VIDEO",										"کیفیت های موجود فیلم");
define("COUNTRY",									"کشور سازنده");
define("GENRES",									"ژانرها");
define("DIRECTOR",									"کارگردان");
define("WRITER",									"نویسنده");
define("PRODUCER",									"تهیه کننده");
define("MUSIC",										"موسیقی");
define("dubbed",										"دوبله شده ؟");
define("sebodi",										"سه بعدی هست ؟");
define("dubbed0",										"دوبله نیست");
define("sebodi0",										"سه بعدی نیست");
define("dubbed1",										"دوبله هست");
define("sebodi1",										"سه بعدی هست");
// Automatic update
define("AUTOUPDATE_INFO",							"فیلم های شما در حال بروزرسانی از سایت مرجع می باشد بنابراین صبور باشید ...");
define("STOP_UPDATE",								"توقف بروزرسانی");
/**
* User management
*/
define("NEW_USER",									"کاربر جدید");
define("EMAIL",										"آدرس ایمیل");
define("AGAIN",										"دوباره");
define("ROLE",										"نقش");
define("GUEST",									  "کاربر معمولی سایت");
define("EDITOR",									"ویرایشگر");
define("ADMIN",										"مدیریت");
define("USERS",										"کاربران");
define("LAST_LOGGED_IN",							"آخرین ورود");
define("DUPLICATE_USER_NAME_OR_EMAIL",				"کاربری با این نام کاربری یا آدرس ایمیل قبلا ثبت نام کرده است .");
/**
 * Messages
 */
define("CONFIRM_REMOVE",							"آیا شما مطمن هستید می خواهید این را حذف کنید ؟");
define("CONFIRM_REMOVE_COVER",						"آیا شما مطمن هستید می خواهید این کاور را حذف کنید ؟");
// Validation
define("VALIDATOR_REQUIRED",						"این فیلد ضروری می باشد.");
define("VALIDATOR_NUMBER",							"لطفا یک شماره صحیح وارد نمایید.");
define("VALIDATOR_DIGITS",							"لطفا یک شماره صحیح وارد نمایید.");
define("VALIDATOR_EMAIL",							"لطفا یک آدرس ایمیل صحیح وارد نمایید.");
define("VALIDATOR_URL",								"لطفا آدرس را صحیح وارد نمایید (http://...) .");
define("VALIDATOR_DATE",							"لطفا تاریخ را همانند الگو وارد کنید (روز-ماه-سال) .");
define("VALIDATOR_ACCEPT_JPG",						"از فرمت صحیح استفاده کنید (jpg) .");
define("VALIDATOR_EQUAL_TO",						"هر دو مقدار باید یکی باشد .");
// Footer
define("footer_msg",						"تمام حقوق این سایت محفوظ بوده و هرگونه کپی برداری بدون ذکر منبع پیگرد قانونی خواهد داشت . طراحی و برنامه نویسی شده از سید امیرحسین طاووسی");
?>